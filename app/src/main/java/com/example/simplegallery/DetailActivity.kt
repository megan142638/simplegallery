package com.example.simplegallery

import android.app.PendingIntent
import android.app.RecoverableSecurityException
import android.content.Intent
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import com.davemorrissey.labs.subscaleview.ImageSource
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView
import com.example.simplegallery.databinding.ActivityDetailImageBinding
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class DetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailImageBinding
    // define the touch view
    lateinit var imageView : SubsamplingScaleImageView

    var imageName: String? = null
    var imageUri: String? = null

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailImageBinding.inflate(layoutInflater)
        setContentView(binding.root)

        imageUri = intent.getStringExtra("IMAGE_URI")
        imageName = intent.getStringExtra("IMAGE_NAME")

        imageView = binding.imageDetailView
        imageView.setImage(ImageSource.uri(imageUri!!))
        val originRotation = getPhotoOrientation(imageUri!!)
        imageView.rotation = originRotation.toFloat()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun getPhotoOrientation(uri:String): Int {
        var rotate = 0
        val inputStream = this.contentResolver.openInputStream(uri.toUri())
        val exif = inputStream?.let { ExifInterface(it) }
        val orientation = exif?.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)

        when(orientation){
            ExifInterface.ORIENTATION_ROTATE_270 ->{
                rotate = 270
            }
            ExifInterface.ORIENTATION_ROTATE_180 ->{
                rotate = 180
            }
            ExifInterface.ORIENTATION_ROTATE_90 ->{
                rotate = 90
            }
        }
        Log.d("originRotate:",rotate.toString())
        return rotate
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_detail, menu);
        return true;
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.action_home -> {
                backToMain()
            }

            R.id.action_share -> {
                shareImage()
            }

            R.id.action_delete -> {
                requestDeletePermission()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun shareImage(){
        val contentUri = imageUri?.toUri()
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.type = "image/*"
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject Here") //for sharing with email apps
        shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri)
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivity(Intent.createChooser(shareIntent, "Share Via"))
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    private fun requestDeletePermission(){
        val uri = imageUri?.toUri()

        try{
            contentResolver.delete(uri!!, null, null)
        }catch (e:SecurityException){
            var pendingIntent : PendingIntent? = null
            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.R){
                val uris : ArrayList<Uri>? = null
                if (uri != null) {
                    uris?.add(uri)
                }
                pendingIntent = uris?.let { MediaStore.createDeleteRequest(contentResolver, it) }
            }else{
                if(e is RecoverableSecurityException){
                    pendingIntent = e.userAction.actionIntent
                }
            }

            if(pendingIntent!=null){
                val intentSender = pendingIntent.intentSender
                startIntentSenderForResult(intentSender,100,null,0,0,0)
                deleteImage()
            }else{
                Log.d("fuck","fuck")
            }
        }
    }

    private fun deleteImage(){
        val uri = imageUri?.toUri()
        val dialogDF = MaterialAlertDialogBuilder(this)
            .setTitle(getString(R.string.deletedialog))
            .setMessage(imageUri.toString())
            .setPositiveButton(getString(R.string.yesdialog)){self,_ ->
                if(uri != null){
                    contentResolver.delete(uri, null, null)
                    backToMain()
                }else{
                    Toast.makeText(this,getString(R.string.deniedtoast),Toast.LENGTH_SHORT).show()
                }
            }
            .setNegativeButton(getString(R.string.nodialog)){self, _ ->
                self.dismiss()
            }
        dialogDF.create().show()
    }

    private fun backToMain(){
        val intent = Intent(this,MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }
}