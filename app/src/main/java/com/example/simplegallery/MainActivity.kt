package com.example.simplegallery

import android.Manifest
import android.annotation.SuppressLint
import android.content.ContentResolver
import android.content.ContentUris
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.database.ContentObserver
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.simplegallery.databinding.ActivityMainBinding
import java.util.*

class MainActivity : AppCompatActivity() {

    private val images = mutableListOf<ImagesStore>()
    private lateinit var binding: ActivityMainBinding
    private var contentObserver: ContentObserver? = null

    companion object{
        const val REQUEST_IMAGES = 100
        const val REQUEST_CAMERA = 200
    }

    // define Observer
    class MyObserver(handler: Handler?, val mainActivity: MainActivity) : ContentObserver(handler) {
        override fun onChange(selfChange: Boolean) {
            super.onChange(selfChange)
            Log.d("onChange: ","onChange")
            mainActivity.readImages()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Determine whether the app currently has the permission.
        havePermission()

        // registe Observer
        val contentObserver: ContentObserver = MyObserver(Handler(), this)
        contentResolver.registerContentObserver(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, true, contentObserver)

    }

    // After click the allow permission, app run this function automatically.
    // make sure the permission choice from android framework.
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when(requestCode){
            REQUEST_IMAGES -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // select all photo from Provider
                    readImages()
                } else {
                    AlertDialog.Builder(this)
                        .setMessage(getString(R.string.permissiondialog))
                        .setPositiveButton("OK",null)
                        .show()
                }
                return
            }
        }
    }

    private fun havePermission(){
        val permission = ActivityCompat.checkSelfPermission(this,
            Manifest.permission.READ_EXTERNAL_STORAGE
        )

        // if don't get the permission.
        if (permission != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ),REQUEST_IMAGES)
        }else{
            // select all photo from Provider
            readImages()
        }
    }

    @SuppressLint("Range")
    private fun readImages() {
        images.clear()

        val projection = arrayOf(
            MediaStore.Images.Media._ID,
            MediaStore.Images.Media.DISPLAY_NAME,
            MediaStore.Images.Media.DATE_ADDED
        )
        val sortOrder = "${MediaStore.Images.Media.DATE_ADDED} DESC"
        val cursor = contentResolver.query(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            projection,
            null,
            null,
            sortOrder
        )

        cursor?.run {
            while (cursor.moveToNext()) {
                val id = cursor.getLong(cursor.getColumnIndex(MediaStore.Images.Media._ID))
                val dateModified = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATE_ADDED))
                val displayName = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME))
                val contentUri = ContentUris.withAppendedId(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    id
                )
                val image = ImagesStore(id, displayName, dateModified,contentUri)
                images += image
            }

            Log.d("image: ",images.toString())
            Log.d("image: ",images.size.toString())

            // show all photo
            showImages()
            cursor.close()
        }
    }

    private fun showImages(){
        // get recyclerview component & setting
        // height、width no change
        binding.recycler.setHasFixedSize(true)
        binding.recycler.layoutManager = GridLayoutManager(this, 3)
        setRecycleViewAdapter()
    }

    private fun setRecycleViewAdapter(){
        class ContactViewHolder(view: View) : RecyclerView.ViewHolder(view){
            val imageView: ImageView = view.findViewById(R.id.image)
        }

        var onImageClick : ((ImagesStore) -> Unit)? = null

        val adapter = object : RecyclerView.Adapter<ContactViewHolder>() {
            // find a view(gallery_layout.xml) which can display all photo
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
                    : ContactViewHolder {
                val view = layoutInflater.inflate(R.layout.gallery_layout, parent, false)
                return ContactViewHolder(view)
            }

            // get the data size
            override fun getItemCount(): Int {
                return images.size
            }

            override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
                //Log.d("position: ", position.toString())
                // imager loader
                Glide.with(holder.imageView)
                    // where load from
                    .load(images[position].contentUri)
                    .centerCrop()
                    .into(holder.imageView)

                holder.imageView.setOnClickListener {
                    onImageClick?.invoke(images[position])
                }
            }
        }

        onImageClick = {
            val intent = Intent(this,DetailActivity::class.java)
            intent.putExtra("IMAGE_URI",it.contentUri.toString())
            intent.putExtra("IMAGE_NAME",it.displayName)
            startActivity(intent)
        }

        binding.recycler.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.action_camera -> {
                openCamera()
                readImages()
            }

            R.id.switchGrid2 -> {
                binding.recycler.layoutManager = GridLayoutManager(this,2)
                setRecycleViewAdapter()
            }

            R.id.switchGrid3 -> {
                binding.recycler.layoutManager = GridLayoutManager(this,3)
                setRecycleViewAdapter()
            }

            R.id.switchListView -> {
                binding.recycler.layoutManager = LinearLayoutManager(this)
                setRecycleViewAdapter()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun openCamera() {
        val camera = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val values = ContentValues().apply {
            put(MediaStore.Images.Media.TITLE,"My Picture")
            put(MediaStore.Images.Media.DESCRIPTION,"Testing")
        }
        val imageUri = contentResolver.insert(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,values
        )
        camera.putExtra(MediaStore.EXTRA_OUTPUT,imageUri)
        startActivityForResult(camera, REQUEST_CAMERA)
    }

    // unregister Observer
    override fun onDestroy() {
        super.onDestroy()
        contentObserver?.let {
            contentResolver.unregisterContentObserver(it)
        }
    }

    // after launching camera through this app, this app wiil reload the MainActivity to update provider
//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if(requestCode == REQUEST_CAMERA){
//            if(resultCode == Activity.RESULT_OK){
//                val intent = Intent(this,MainActivity::class.java)
//                startActivity(intent)
//            }
//        }
//    }

    // After stopping this app and launch camera to take photo, reload the page to update provider
//    override fun onRestart() {
//        super.onRestart()
//        val intent = Intent(this,MainActivity::class.java)
//        startActivity(intent)
//    }
}






