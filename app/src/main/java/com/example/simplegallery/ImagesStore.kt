package com.example.simplegallery

import android.net.Uri
import androidx.recyclerview.widget.DiffUtil

data class ImagesStore (
    val id: Long,
    val displayName: String,
    val dateAdded: String,
    val contentUri: Uri
){
    companion object {
        val DiffCallback = object : DiffUtil.ItemCallback<ImagesStore>() {
            override fun areItemsTheSame(oldItem: ImagesStore, newItem: ImagesStore) =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: ImagesStore, newItem: ImagesStore) =
                oldItem == newItem
        }
    }
}